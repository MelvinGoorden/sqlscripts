USE ModernWays;
SET SQL_SAFE_UPDATES = 0;

DELETE FROM huisdieren
WHERE Baasje = 'Thaïs' AND Soort = 'hond';

DELETE FROM huisdieren
WHERE Baasje = 'Truus' AND Soort = 'kat';

SET SQL_SAFE_UPDATES = 1;