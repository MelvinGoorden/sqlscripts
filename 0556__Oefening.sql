USE modernways;

SELECT huisdieren.Naam, baasjes.Naam
FROM huisdieren
CROSS JOIN baasjes
WHERE baasjes.huisdieren_Id = huisdieren.Id;