USE modernways;

CREATE TABLE Student(
    Voornaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
    Familienaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
    studentennummer INT AUTO_INCREMENT,
    PRIMARY KEY (studentennummer)
);

CREATE TABLE Opleiding(
    Naam VARCHAR(100) CHAR SET utf8mb4,
    PRIMARY KEY (Naam)
);

CREATE TABLE Vak(
    Naam VARCHAR(100) CHAR SET utf8mb4,
    PRIMARY KEY (Naam)
);

CREATE TABLE Lector(
	Voornaam VARCHAR(100) CHAR SET utf8mb4,
    Familienaam VARCHAR(100) CHAR SET utf8mb4,
    Personeelsnummer INT AUTO_INCREMENT,
    PRIMARY KEY (Personeelsnummer)
);

ALTER TABLE Student
ADD COLUMN Semester tinyint unsigned NOT NULL,
ADD COLUMN Opleiding_Naam VARCHAR(100) CHAR SET utf8mb4,
ADD CONSTRAINT fk_Student_Opleiding
  FOREIGN KEY (Opleiding_Naam)
  REFERENCES Opleiding(Naam);

CREATE TABLE Opleidingsonderdeel(
	Semester tinyint unsigned NOT NULL PRIMARY KEY,
    Opleiding_Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
	CONSTRAINT fk_Opleidingsonderdeel_Opleiding
		FOREIGN KEY (Opleiding_Naam)
		REFERENCES Opleiding(Naam),
	Vak_Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
	CONSTRAINT fk_Opleidingsonderdeel_Vak
		FOREIGN KEY (Vak_Naam)
		REFERENCES Vak(Naam)
);

CREATE TABLE LectorGeeftVak(
	Lector_Personneelsnummer INT NOT NULL,
    CONSTRAINT fk_LectorGeeftVak_Lector
		FOREIGN KEY (Lector_Personneelsnummer)
		REFERENCES Lector(Personeelsnummer),
    Vak_Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
	CONSTRAINT fk_LectorGeeftVak_Vak
		FOREIGN KEY (Vak_Naam)
		REFERENCES Vak(Naam)
);



  
  
