USE modernways;

ALTER TABLE baasjes
ADD COLUMN huisdieren_Id INT,
ADD CONSTRAINT fk_Baasjes_huisdieren
  FOREIGN KEY (huisdieren_Id)
  REFERENCES huisdieren(Id);